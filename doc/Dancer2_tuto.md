% Tutorial de Dancer2
% Sergio Alvariño <salvari@gmail.com>
% Marzo - 2015

# Dancer2 un tutorial rápido por BricoLabs  #

### Un tutorial rápido para dar los primeros pasos con Dancer2  ###

Una copia descarada del [Tutorial de
Dancer2](https://metacpan.org/pod/Dancer2::Tutorial)

# ¿De que va esto?

Este tutorial se inspira en el tutorial de Dancer2 disponible en
CPAN. En principio la idea es intentar hacer una comparativa de
distintos micro web frameworks, y como casi siempre trabajo con Perl
empezamos por [Dancer2](https://metacpan.org/pod/Dancer2).

Esto **no es** un tutorial de Perl, solo pretendemos ver como se usa
Dancer2 para desarrollar una aplicación web sencilla.

Si alguien está interesado en profundizar en Perl le recomiendo que se
lea el libro [Modern Perl](http://modernperlbooks.com/). Es cortito,
disponible online para descarga gratuita, fácil de entender y muy
actualizado. Te presenta todo el lenguaje Perl y sus posibilidades

Como dijimos antes, nos inspiramos en (y copiamos de) el tutorial
original de Dancer2 que está basado a su vez en los tutoriales
escritos para [Flask](http://flask.pocoo.org/) y
[Bottle](http://bottlepy.org/docs/dev/index.html). Parece que en esos
dos tutoriales se desarrollan aplicaciones sencillas de micro
blogging.

Para darle un poco de emoción e interés al tutorial vamos a pasar de
hacer un micro blogging y vamos a desarrollar una aplicación de
**¡BricoPuntos!**

## Los BricoPuntos

La idea original propuesta por Tucho (@procastino) era implementar un
sistema de BricoPuntos en la asociación de BricoLabs.

Cada miembro de BricoLabs empezaría el año con un cierto número de
BricoPuntos en su cuenta. A lo largo del año podría asignar
BricoPuntos a otros miembros a modo de recompensa cuando estos hacen
algo de valor para la asociación. Una forma de dinamización por
gammification.

El consenso general es que los BricoPuntos deberían implantarse en
BricoLabs, personalmente creo que si no los usamos todavía es por que
mantener una cuenta de BricoPuntos en una hoja de cálculo les quita
casi todo el atractivo.


# ¿Qué es Dancer2?

Dancer es una copia de [Sinatra](http://www.sinatrarb.com/), un micro
web framework en Ruby. Igual que Sinatra, Dancer permite el desarrollo
rápido de aplicaciones web mediante la construcción de un conjunto de
verbos http, URL (llamadas rutas) y métodos programados para tratar el
tráfico dirigido a esas URL o rutas.

Dancer2 es la segunda generación de Dancer

* El Dancer original ha sido completamente re-escrito y ahora está
  basado en Moo (un subconjunto sencillo de
  [Moose](https://metacpan.org/pod/Moose), el sistema de programación
  orientada a objetos de Perl más actualizado
* Dancer ahora es una
  aplicación "[FatPackable](https://metacpan.org/pod/App::FatPacker)"
  (luego explicamos esto)

## Un ejemplo mínimo

```perl
use Dancer2;
 
get '/' => sub {
  return 'Hello World!';
};
 
start;
```

En este ejemplo mínimo de una aplicación web hecha con Dancer2 vemos
que definimos un único verbo HTTP "GET" asociado a la URL root "/"

Si ejecutamos este programa nos va a mostrar un "Hello world" sin mas
que apuntar nuestro navegador a http://localhost:3000


# Módulos Perl requeridos

Como mínimo necesitaremos:

* [Dancer2](https://metacpan.org/pod/Dancer2)
* [Template::Toolkit](https://metacpan.org/pod/Template)
* [DBD::SQLite](https://metacpan.org/pod/DBD::SQLite)

Tampoco está de más instalar el Dancer original (ya veremos para que)

* [Dancer](https://metacpan.org/pod/Dancer)

También tenemos que tener instalado [SQLite](https://sqlite.org/) en
nuestro sistema para hacer el desarrollo, aunque podríamos usar
cualquier otra base de datos como veremos después.

Los módulos de Perl se pueden instalar usando cpan:

```bash
cpan Dancer Dancer2 Template File::Slurp DBD::SQLite
```

# Un ejemplo más completo: Aplicación de BricoPuntos


## Filosofía de la aplicación{#MVC}

Vamos a seguir en lineas muy generales un modelo
[MVC](http://es.wikipedia.org/wiki/Modelo%E2%80%93vista%E2%80%93controlador).

Esta arquitectura postula que el software se estructura en tres componentes:

Model (Modelo)

:    Es la representación de la información con la que opera el sistema,
     en nuestro caso será una base de datos con las tablas necesarias
     para soportar los datos de nuestra aplicación de **BricoPuntos**

View (Vista)

:    Es la interfaz hacia el usuario, que en nuestro caso se hará via web
     y quedará encapsulada en Templates (plantillas), esto se verá más
     claro a lo largo del tutorial.

Controller (Controlador)

:    El controlador es el midleware que media entre la vista y el modelo,
     procesa las peticiones del usuario, actualiza el modelo,
     implementa la lógica de la aplicación etc. En nuestra aplicación
     el controlador será el script Perl de nuestra aplicación.

## La base de datos

Nuestro modelo se almacenará en una base de datos, podemos usar la que
queramos, para no liarnos mucho usaremos SQLite para el desarrollo.

Nuestra base de datos, a primera vista, debería hacer dos cosas:

- Almacenar un listado de miembros de BricoLabs con su saldo de BricoPuntos
- Almacenar las transacciones de BricoPuntos realizadas

Tiene toda la pinta de que nos podemos apañar con dos tablas, digamos
"*usrpoints*" y "*transact*"

La definición de las tablas sería algo así:

```sql
DROP TABLE IF EXISTS usrpoints;
DROP TABLE IF EXISTS usrpoints;
CREATE TABLE usrpoints(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  login      TEXT,    -- login
  passwd     TEXT,    -- password
  own_points INTEGER, -- own points
  won_points INTEGER  -- won points
);

DROP TABLE IF EXISTS transact;
CREATE TABLE transact(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  usr_from   TEXT,    -- the user giving points
  usr_to     TEXT,    -- the user receiving points
  points     INTEGER, -- transferred points
  reason     TEXT     -- a reason for this transaction
);
```

La definición de la base de datos la guardamos en el fichero *sql/bpointsdb.sql*

Las tablas son bastante sencillas (de momento) pero hay que comentar dos
cosas:

El campo id

:    En casi todo los web frameworks es muy recomendable, y a veces
     imprescindible, contar con una clave primaria automática y que
     suele llamarse *id*

Formas normales

:    Los puristas de las bases de datos podrían decir que no está
     normalizada y que los campos *usr_from* y *usr_to* deberían ser
     referencias a la tabla *usrpoints*. Pero creo que así las consultas
     quedarán más claras.


Vamos a preparar un fichero con unos datos iniciales que nos permitan
hacer pruebas:

```sql
INSERT INTO usrpoints (login, own_points, won_points)
VALUES(
  ('countzero', 1000, 0),
  ('idoru'    , 1000, 0),
  ('sphinx'   , 1000, 0),
  ('pr0b3'    , 1000, 0)
);
```

Y lo guardamos en el fichero *sql/bpdata.sql*

Ahora podemos probarlo todo desde un terminal:

```bash
$ cd sql
$ sqlite3 bpoints.db
sqlite> .read bpointsdb.sql
sqlite> .tables
transact   usrpoints
sqlite> .read bpdata.sql
sqlite> select * from usrpoints;
1|countzero|1000|0
2|idoru|1000|0
3|sphinx|1000|0
4|pr0b3|1000|0
sqlite> .quit
```

* Invocamos a sqlite3 con la base de datos *bpoints.db*
* Ejecutamos los ficheros: *bpointsdb.sql* para crear las tablas y *bpdata.sql* para insertar datos
* Con el comando *.tables* podemos ver las tablas definidas en la base de datos
* Hacemos un select para comprobar los datos cargados en la tabla usrpoints

Ya tenemos una base de datos con algunos datos para empezar nuestra aplicación.

La base de datos y su definición constituyen la parte **Model** de la arquitectura [MVC](#MVC).

## Empezando con la aplicación

### Conexión a la base de datos

Vamos a añadir al código de nuestra aplicación una función para
conectarnos a la base de datos.

```perl
sub connect_db {
  my $dbh = DBI->connect("dbi:SQLite:dbname=".setting('database')) or
     die $DBI::errstr;
 
  return $dbh;
}
```

La función nos devuelve un *database handler* para que podamos hacer
las operaciones sobre base de datos.

Hay plugins de Dancer2 disponibles para facilitar el acceso a la base
de datos
([Dancer2::Plugin::Database](https://metacpan.org/pod/Dancer2::Plugin::Database)),
pero de momento vamos a intentar el desarrollo de la aplicación sin
recurrir a ellos.

### Escribiendo la primera ruta

Vamos a escribir la primera ruta de nuestra aplicación. 

Vamos a diseñar nuestra aplicación para que la página raiz nos muestre
el ranking de participantes, los que tengan más bricopuntos ganados
primero.

```perl
get '/' => sub {
  my $db = connect_db();
  my $sql = 'select login, own_points, won_points from usrpoints order by won_points desc';
  my $sth = $db->prepare($sql) or die $db->errstr;
  $sth->execute() or die $sth->errstr;
  template 'show_ranking.tt', {
     'entries' => $sth->fetchall_arrayref({}),
  };
};
```

La ruta definida se encargará de atender las peticiones "get" en la
url raiz '/' y le asociamos una función que:

* Obtiene una conexión a la base de datos
* Prepara una consulta para obtener el listado de participantes
* Obtiene el listado
* Y lo pasa como parámetro al template *show_ranking.tt* que es el que
  Dancer va a utilizar para servir la petición

Lo que estamos programando ahora es la parte de **Controller** de nuestra
arquitectura MVC.

Lo que vemos es más o menos Perl clásico, con el típico ciclo para
hacer una consulta a base de datos.

[DBI](https://metacpan.org/pod/DBI) es el módulo de Perl que encapsula
las funciones de acceso a cualquier base de datos relaccional. DBI
ofrece muchas formas de devolver los resultados, como en nuestra
consulta hemos ordenado ya los resultados por el campo "*won_points*"
queremos que nos lo devuelva como un array (que es una lista
ordenada). Como además queremos poder referirnos a los campos de cada
registro devuelto por el nombre del campo le pedimos a DBI que nos
devuelva un array de referencias a hash (simplemente metiendo una
referencia a hash vacia como parámetro de *fetchall_arrayref*).

Para entendernos nos va a devolver esto:

```perl
[{login => 'countzero', own_points => 1000, won_points => 0},
 {login => 'idoru'    , own_points => 1000, won_points => 0},
 {login => 'sphinx'   , own_points => 1000, won_points => 0},
 {login => 'pr0b3'    , own_points => 1000, won_points => 0},
]
```

La parte novedosa es la llamada a *template*.

### El primer template

En este tutorial vamos a usar el [Template
Toolkit](https://metacpan.org/pod/Template), el mítico módulo de Perl
para tratamiento de plantillas, que nos da mucha más potencia y
flexibilidad que el que viene por defecto con Dancer2.

En Dancer todos los templates van en el directorio *views/*
opcionalmente se puede crear una plantilla *layout*, que debe estar en
el directorio *views/layout/* para dar un "*look and feel*" consistente
a todas las vistas de nuestra aplicación. En este tutorial crearemos
más adelante un layout que llamaremos *main.tt*

El segundo argumento a la directiva template es una referencia a un
hash (un diccionario o array asociativo). En este hash hay que poner
los parámetros que queramos pasar a nuestro template. En este caso el
único parámetro que pasamos a nuestro template es otra referecia a un
hash que nos devuelve el módulo DBI con los resultados de nuestra
consulta a la base de datos.

Nuestro template *show_ranking.tt*:

```html
[% IF entries.size %]
<table class="zebra" cellspacing="0" cellpadding="5" border="1">
  <caption>Ranking de BricoPuntos</caption>
  <tr>
    <th>Login</th>
    <th>Puntos Ganados</th>
    <th>Puntos Poseídos</th> 
  </tr>

  [% FOREACH line IN entries %]
  <tr class="[% IF loop.index % 2 %]even[% ELSE %]odd[% END %]">
    <td>[% line.login      %]</td>
    <td>[% line.won_points %]</td>
    <td>[% line.own_points %]</td>
  </tr>
  [% END %]
</table>
[% ELSE %]
  <para><em>Unbelievable. No entries here so far</em></para>
[% END %]
```

Como podéis ver las directivas para el Template Toolkit van siempre
encerradas entre [% %] (aunque esto es configurable y se puede
cambiar)

Nuestro template no hace gran cosa por el momento. Hemos decidido
presentar los resultados en una tabla y hemos asignado clases a la
tabla y cada linea de la tabla para ponerla un poco más presentable
con css (más tarde) pero son perfectamente prescindibles.

Los templates nos sirven para implementar la parte **View** de la
arquitectura MVC.

## Primera prueba

Con todo lo que hemos visto hasta ahora ya podemos montar una
aplicación completamente funcional.

```perl

#!/usr/bin/perl
use Dancer2;
use DBI;

set 'database'     => 'sql/bpoints.db';
set 'template'     => 'template_toolkit';
set 'logger'       => 'console';
set 'log'          => 'debug';
set 'show_errors'  => 1;
set 'startup_info' => 1;
set 'warnings'     => 1;

sub connect_db {
  my $dbh = DBI->connect("dbi:SQLite:dbname=".setting('database')) or
    die $DBI::errstr;

  return $dbh;
}

get '/' => sub {
  my $db = connect_db();
  my $sql = 'select login,
                    own_points,
                    won_points
               from usrpoints
             order by won_points desc';
  my $sth = $db->prepare($sql) or die $db->errstr;
  $sth->execute() or die $sth->errstr;
  template 'show_ranking.tt', {
    'entries' => $sth->fetchall_arrayref({}),
  };
};

start;

```

El código de nuestra aplicación lo dejamos en el fichero
*AppBricoPoints/bricopoints.pl*

Lo único que hemos añadido para tener nuestra aplicación funcionando ha sido:

- Importar los módulos necesarios en nuestro programa
    -    *Dancer2* evidentemente
    -    *DBI* Para acceder a la base de datos
- Establecer algunas opciones para Dancer2
    - Especificamos que vamos a usar Template Toolkit para generar las
      páginas web a partir de los templates
    - Especificamos opciones para que el Logger nos informe de los
      errores por consola
    - Almacenamos el nombre de la base de datos que vamos a usar
      (aunque Dancer2 no va a hacer nada con la db)

- Añadimos al final la orden *start* que pone en marcha a Dancer

No tenemos más que dar privilegios de ejecución e invocar el programa
desde la terminal

```bash
$ chmod 744 bricopoints.pl
$ ./bricopoints.pl
>> Dancer2 v0.154000 server 13849 listening on http://0.0.0.0:3000
```

Y ya podemos ver nuestra aplicación en marcha sin más que apuntar
nuestro navegador a <http://localhost:3000>

Por el momento es una aplicación bastante tonta, pero ya funciona.

# Mejorando el aspecto de la aplicación

De momento nuestra aplicación es super simple: solo nos deja ver la
lista de participantes. Y además tiene unas pintas horribles.

![Una pinta horrible](FirstList.png)

Vamos a mejorar un poco el aspecto antes de seguir el desarrollo de la
aplicación usando hojas de estilos CSS y un *layout*.

# Ficheros estáticos

Hay que explicar que los ficheros estáticos en la aplicación Dancer
residen en el directorio *public/* Si quisiéramos servir páginas
estáticas, estas estarían en *public/*

```perl
get '/' => sub {
   send_file 'index.html';
};
```

En el ejemplo anterior el fichero *index.html* estaría en el
directorio *public/*

Otros ficheros estáticos como por ejemplo las hojas de estilos también
deben residir en *public*. Concretamente en *public/css/* para tener
las cosas bien ordenadas.

Al hacer referencia desde la aplicación a un fichero en el directorio
*public/* omitimos ese directorio. Por ejemplo, dentro de la
aplicación, nos referimos a la hoja de estilos en la ruta
*public/css/style.css* como *css/style.css*

# Layout

Ya habíamos mencionado que Dancer2 nos permite usar un *layout*, y que
nosotros íbamos a llamar *main.tt* a nuestro layout.

El *layout* pertenece al ecosistema de *views* y tiene su propio
directorio *views/layout/*

Para usar nuestro fichero layout *main.tt* tenemos que declararlo en
la aplicación añadiendo a *bricopoints.pl* la linea:

```perl
set layout => 'main';
```

Esto hará que cada vez que Dancer procese un template busque el
*layout* en el fichero *views/layout.main.tt*, lo procese, e inserte
el resultado del template en lugar del *[% content %]* del
layout.

Nuestro *main.tt* tiene esta pinta:

```html
<!doctype html>
<html>
  <head>
    <title>BricoPuntos</title>
    <link rel=stylesheet type=text/css href="[% css_url %]">
  </head>
  <body>
    <div class=page>
      <h1>BricoPuntos</h1>
      <div class=metanav>
        <a href="[% ranking_url    %]">Ver clasificación</a>
        <a href="[% transfer_url   %]">Hacer transferencias</a>
      </div>
      [% content %]
    </div>
  </body>
</html>
```

Vemos que el resultado de procesar los templates se inserta en la
cuarta linea empezando por abajo en lugar del parámetro *content*

Pero ¿que significan todas los parámetros terminados en *\_url*?

## Usando *hooks*: *before_template_render*

Dancer permite definir *hooks* que son subrutinas que son llamadas en
momentos específicos. Estas rutinas permiten minimizar la duplicación
de código y resultan extremadamente útiles (más adelante veremos más
usos).

Para este caso concreto vamos a usar el hook: *before_template_render*
definiéndolo como sigue:

```perl
hook before_template_render => sub {
   my $tokens = shift;
 
   $tokens->{'css_url'     } = request->base . 'css/style.css';
   $tokens->{'ranking_url' } = uri_for('/');
   $tokens->{'transfer_url'} = uri_for('/transfer');
};
```

Usando la función *uri_for* evitamos dejar las rutas *hardcoded* y en
caso de que las rutas cambien cuando hagamos la instalación definitiva
de la aplicación no tendremos que editar el código

# Hoja de estilos

No vamos a hacer muchas florituras con nuestros estilos, así que los
definimos así:

```css
body { font-family: sans-serif;
       background: #eee; }

a, h1, h2 { color: #377BA8; }

h1, h2 { font-family: 'Arial', serif;
         margin: 0; }

h1 { border-bottom: 2px solid #eee; }
h2 { border-bottom: 1px solid #eee;
     font-size: 1.2em; }
.page { margin: 2em auto;
        width: 70em;
        border: 5px solid #ccc;
        padding: 0.8em;
        background: white; }

.metanav { text-align: left;
           font-size: 0.8em;
           padding: 0.3em;
           margin-bottom: 1em;
           background: #fafafa; }

.flash { background: #CEE5F5;
         padding: 0.5em;
         border: 1px solid #AACBE2; }

.error { background: #F0D6D6;
         padding: 0.5em; }

.zebra tbody tr.even td { background-color: #80FFFF; font-size: 0.6em; }
.zebra tbody tr.odd  td { background-color: #BFFFFF; font-size: 0.6em; }
.zebra tbody th { background-color: #009999; font-size: 0.6em; }
```

Una vez que hemos completado el *layout* y añadido la hoja de estilos
el aspecto de la aplicación mejora bastante:

![Nuevo look](NewLook.png)

# Sesiones

Vale. Ya tenemos una aplicación funcionando y con un aspecto aceptable
(al fin y al cabo es solo un tutorial).

Ahora tocaría implementar las transferencias de puntos entre miembros
de BricoLabs; pero para eso sería necesario que los participantes se
identificaran con una contraseña. Eso nos lleva a **las Sesiones de
Usuario**

La capacidad de gestionar sesiones de usuario es básica para poder
hacer una aplicación web. Como cualquier framework que se precie
Dancer integra facilidades para gestionar sesiones y multitud de
plugins para realizar la autenticación de varias maneras: LDAP, PAM,
SSH, POP3, etc. (Ver por ejemplo
[Authen::Simple](https://metacpan.org/pod/Authen::Simple))

Como estamos en modo tutorial y en su momento decidimos no encapsular
en un plugin las operaciones de base de datos, vamos a implementar la
autenticación contra la base de datos.

Usaremos el módulo Perl
[Crypt::SaltedHash](https://metacpan.org/pod/Crypt::SaltedHash) por
que ni siquiera en un tutorial se deben almacenar las password en
claro.

## Limitando el acceso a rutas

Lo primero que vamos a hacer es asegurarnos de que un usuario no
autenticado no pueda ver nada más que el ranking.

Un truco habitual es hacer esto en el *before_template_render*. El
hook modificado nos quedaría así:

```perl
hook before_template => sub {
  my $tokens = shift;

  $tokens->{'css_url'     } = request->base . 'css/style.css';
  $tokens->{'ranking_url' } = uri_for('/');
  $tokens->{'transfer_url'} = uri_for('/transfer');

  if (!session('user')
      && request->dispatch_path !~ m{^/login}
      && request->dispatch_path !~ m{^/$}) {
    forward '/login', { requested_path => request->dispatch_path };
  }

};
```

La primera parte ya la teníamos de antes para facilitar url a *main.tt*

Ahora, si el usuario es anónimo, y no está solicitando hacer login, y
no está solicitando ver el ranking:

* Lo redirigimos a la página de login
* Y le pasamos al login la página que solicitó el usuario para
  redirigirlo una vez autenticado

Con el nuevo *hook* un usuario no autenticado solo puede visitar la
url "/"

Ahora necesitamos añadir dos nuevas rutas para poder hacer login en la
página web.

La ruta *get* para la url */login* es muy fácil:

```perl
get '/login' => sub {
  template 'login', { path => param('requested_path') };
};
```

El correspondiente template *login.tt* sería:

```html
<form action='/login' method='POST'>
   Usuario: <input type='text' name='user'/>
   Password: <input type='password' name='pass' />
 
   <!-- Put the original path requested into a hidden
        field so it's sent back in the POST and can be
        used to redirect to the right page after login -->
   <input type='hidden' name='path' value='[% path %]'/>
 
   <input type='submit' value='Login' />
</form>
```

# Organización de directorios {#directorios}

AppBricoPoints

:    En este directorio es donde está la aplicación que vamos a programar

     AppBricoPoints/views/

     :    Aquí están las plantillas de las vistas

     AppBricoPoints/views/layout/

     :    Aquí están las plantillas generales de las vistas

     AppBricoPoints/sql/
     
     :     Aquí tenemos ficheros para definir la base de datos y datos de
     prueba

doc/

:    Aqui está la documentación, que es justo esto que estás leyendo, en
     diferentes formatos

tools/

:    Aquí tenemos algunos scripts que nos son de utilidad
     

# Algunas alternativas a Dancer2

Esta sección es completamente subjetiva, y además muy osada, no tengo
suficiente experiencia como para emitir juicios acertados, solo son
opiniones. Pero al menos vale para recordarte que hay un montón de web
frameworks ahí fuera.


* Con Perl

    Mojolicious

    :    Otro micro framework para Perl también basado en rutas
         pero que no tiene DSL (para algunos es una ventaja).

    Jifty

    :    Otro framework muy potente y ligero.
    
    Catalyst

    :    El web framework de Perl de grado industrial, Catalyst no es
         ligero y no está basado en rutas. Instalarlo no es tarea
         trivial, a cambio dispondrás de un framework extremadamente
         potente y capaz de hacer cualquier cosa.

* Con Python

    Flask

    :    Probablemente muy parecido a Dancer, me gustaría re-escribir
         este tutorial con Flask

    Bootle

    :    Otra micro web framework

    Web2py

    :    Un framework **muy interesante** escrito en Python. Aunque se
         sale de la categoría de micro framework si eres Pythonista (o
         incluso si estás aprendiendo) y quieres empezar a desarrollar
         aplicaciones web es muy recomendable. Bien documentado y
         autocontenido, una vez que lo arranques no necesitas ni
         siquiera un editor para ponerte a trabajar.

    Django

    :    El super web framework de Python, al mismo nivel de Catalyst. 
     
# Desplegar el sw en [Koding]()

* Nos registramos en Koding e iniciamos la sesión
* Arrancamos aptitude

```bash
sudo aptitude
```
* Nos aseguramos de quitar la opción "Install recomended packages automatically"
* Salimos del programa y desde linea de comandos:
```bash
sudo aptitude update
sudo aptitude upgrade
```
Con esos dos comandos dejamos nuestro Ubuntu virtual al dia.

A lo largo de la instalación nos preguntará:
* ¿donde instalar Grub?
* ¿mantener el fichero de opciones?
* Una tercera (¿o segunda?) cosa

__NOTA__ Por aquí andamos al 64% de ocupación del disco

Una vez acabado instalamos los paquetes de Ubuntu que necesitamos:

```bash
sudo aptitude install sqlite3
sudo aptitude install libdancer2-perl
sudo aptitude install libdancer2-plugin-database-perl
sudo aptitude build-essential
```

    sqlite3

    :    Para crear y gestionar nuestras bases de datos

    libdancer2-perl

    :    Nos deja instalados Dancer2 y sus dependencias

    libdancer2-plugin-database-perl

    :    Veremos que ventajas nos aporta usar este módulo en el tutorial

    build-essential

    :    Este paquete instala todas las herramientas necesarias para
         compilar C y C++ en nuestro servidor virtual. Hay
         herramientas como "make" que nos harán falta si instalamos
         módulos Perl adicionales desde CPAN.






# Meta

Este documento está escrito en [Pandoc
Markdown](http://johnmacfarlane.net/pandoc/README.html#pandocs-markdown).

El único fichero que hay que editar es *doc/Dancer2_tuto.md*. Una vez
editado el fichero basta ejecutar el script *doc/mkAll* (asegúrate de
estar en el directorio doc para ejecutarlo) y toda la documentación en
los distintos formatos se actualizará.

Con Pandoc es increiblemente fácil traducir el documento original a
multitud de formatos. En particular en este repositorio puedes
encontrar en el directorio doc

- El documento original *dancer2_tuto.md*
- Traducido a mediawiki *dancer2_tuto.mw*
- Traducido a LaTeX *dancer2_tuto.tex*
- Traducido a pdf *dancer2_tuto.pdf*

Además el fichero *README.md* escrito en la versión github de markdown
también se genera a partir del original via Pandoc.

# Como puedo contribuir a este tutorial 

* Cualquier contribución o sugerencia es bienvenida
* Escribe tu propia versión del documento con otro micro web framework
  y compártela

# ¿Con quién puedo hablar de este rollo? 

* Salvari <salvari@gmail.com> un miembro de la tribu BricoLabs
* BricoLabs <bricolabs@gmail.com>
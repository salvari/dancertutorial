#!/usr/bin/perl
use Dancer2;
use DBI;
#use Template;

set 'database'     => 'sql/bpoints.db';
set 'template'     => 'template_toolkit';
set 'logger'       => 'console';
set 'log'          => 'debug';
set 'show_errors'  => 1;
set 'startup_info' => 1;
set 'warnings'     => 1;
set 'layout'       => 'main';

sub connect_db {
  my $dbh = DBI->connect("dbi:SQLite:dbname=".setting('database')) or
    die $DBI::errstr;

  return $dbh;
}

hook before_template => sub {
  my $tokens = shift;

  $tokens->{'css_url'     } = request->base . 'css/style.css';
  $tokens->{'ranking_url' } = uri_for('/');
  $tokens->{'transfer_url'} = uri_for('/transfer');

  if (!session('user')
      && request->dispatch_path !~ m{^/login}
      && request->dispatch_path !~ m{^/$}) {
    forward '/login', { requested_path => request->dispatch_path };
  }

};


get '/' => sub {
  my $db = connect_db();
  my $sql = 'select login, own_points, won_points from usrpoints order by won_points desc';
  my $sth = $db->prepare($sql) or die $db->errstr;
  $sth->execute() or die $sth->errstr;
  template 'show_ranking.tt', {
    'entries' => $sth->fetchall_arrayref({}),
  };
};

get '/login' => sub {
  template 'login', { path => param('requested_path') };
};

post '/login' => sub {
  my $userdata = $DBH->selectrow_hashref('
SELECT login,
       password
  FROM usrpoints
 WHERE login = ?
');
  if (!$user) {
    warning "Failed login for unrecognised user " . params->{user};
    redirect '/login?failed=1';
  } else {
    if (Crypt::SaltedHash->validate($user->{password}, params->{pass})){
      debug "Password correct";
      # Logged in successfully
      session user => $user;
      redirect params->{path} || '/';
    } else {
      debug("Login failed - password incorrect for " . params->{user});
      redirect '/login?failed=1';
    }
  }
};


start;

#!/usr/bin/perl
use Dancer2;
use DBI;
#use Template;

set 'database'     => 'sql/bpoints.db';
set 'template'     => 'template_toolkit';
set 'logger'       => 'console';
set 'log'          => 'debug';
set 'show_errors'  => 1;
set 'startup_info' => 1;
set 'warnings'     => 1;

sub connect_db {
  my $dbh = DBI->connect("dbi:SQLite:dbname=".setting('database')) or
    die $DBI::errstr;

  return $dbh;
}

get '/' => sub {
  my $db = connect_db();
  my $sql = 'select login, own_points, won_points from usrpoints order by won_points desc';
  my $sth = $db->prepare($sql) or die $db->errstr;
  $sth->execute() or die $sth->errstr;
  template 'show_ranking.tt', {
    'entries' => $sth->fetchall_arrayref({}),
  };
};

start;

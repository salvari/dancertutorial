-- MariaDb or MySQL version
-- DROP TABLE IF EXISTS usrpoints;
-- CREATE TABLE usrpoints(
--   id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
--   login VARCHAR(16),  -- login
--   own_points INTEGER, -- own points
--   won_points INTEGER  -- won points
-- );
-- 
-- DROP TABLE IF EXISTS transact;
-- CREATE TABLE transact(
--   id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
--   usr_from   VARCHAR(16),  -- the user giving points
--   usr_to     VARCHAR(16),  -- the user receiving points
--   points     INTEGER,      -- transferred points
--   reason     VARCHAR(256)  -- a reason for this transaction
-- );

DROP TABLE IF EXISTS usrpoints;
CREATE TABLE usrpoints(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  login      TEXT,    -- login
  passwd     TEXT,    -- password
  own_points INTEGER, -- own points
  won_points INTEGER  -- won points
);

DROP TABLE IF EXISTS transact;
CREATE TABLE transact(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  usr_from   TEXT,    -- the user giving points
  usr_to     TEXT,    -- the user receiving points
  points     INTEGER, -- transferred points
  reason     TEXT     -- a reason for this transaction
);
